createRepo = require('./create-bitbucket-repo')

prompt = require('prompt')
optimist = require('optimist')
{ mapValues } = require('lodash')

paramDescriptions = 
  user: 'Bitbucket username'
  pass: 'Bitbucket password'
  repo: 'Repository name'
  owner: 'Repository owner (defaults to username)'

optimist
  .usage('Create a private bitbucket repository.\nUsage: $0')
  .describe('user', paramDescriptions.user)
  .describe('pass', paramDescriptions.pass)
  .describe('repo', paramDescriptions.repo)
  .describe('owner', paramDescriptions.owner)

if optimist.argv.h || optimist.argv.help
  optimist.showHelp()
  return

prompt.override = optimist.argv

prompt.start()

promptOptions =
  properties:
    user:
      message: paramDescriptions.user
      required: true
    pass:
      message: paramDescriptions.pass
      required: true
      hidden: true
    repo:
      message: paramDescriptions.repo
      required: true
    owner:
      message: paramDescriptions.owner
      required: false


prompt.get promptOptions, (err, resultWithEmptyStrings) ->

  result = mapValues(resultWithEmptyStrings, (val) -> if val?.length <= 0 then null else val)

  createRepo result.repo, result.user, result.pass, { owner: result.owner }, (err, result) ->
    if err then return console.error(err)
    console.log("Great! Now do:")
    console.log("git remote add origin #{result}")
    console.log("git push -u origin --all")
