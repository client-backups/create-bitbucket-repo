superagent = require('superagent')

resourceUriToGitUri = (resourceUri) ->
  repoAddress = resourceUri.substr("/2.0/repositories/".length, resourceUri.length)
  "git@bitbucket.org:#{repoAddress}.git"

module.exports = (repoName, user, pass, opts = {}, callback = (->)) ->
  opts.owner ?= user
  opts.repo_slug ?= repoName
  opts.is_private ?= true

  superagent
    .post("https://api.bitbucket.org/2.0/repositories/#{opts.owner}/#{repoName}")

    .send(opts)
    .auth(user, pass)
    .end (err, result) ->
      if err || result.error then callback(err || result.error)
      else callback(null, resourceUriToGitUri(result.body.resource_uri))